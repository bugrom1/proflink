import { DecisionsScreen } from "../../components/bus/decisions/DecisionsScreen";
import { api } from "../../helpers/api";

export default function Index({ decisions }) {
  return <DecisionsScreen decisions={decisions} />;
}

export async function getServerSideProps({ query, req }) {
  const decisions = await api.decisions.fetch();
  return {
    props: { decisions },
  };
}
