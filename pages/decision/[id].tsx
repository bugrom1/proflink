import { Decision } from "../../components/bus/decisions/dicision/Decision";
import { api } from "../../helpers/api";

export default function Index({ decision }) {
  return <Decision decision={decision} />;
}

export async function getServerSideProps({ query, req }) {
  /*const response = await fetch(
    `http://localhost:4000/api/decisions/${query.id}`
  );*/
  const decision = await api.decision.fetch(query.id);
  return {
    props: { decision, query },
  };
}
