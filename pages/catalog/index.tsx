import { CatalogScreen } from "../../components/bus/catalogs/CatalogsScreen";
import { api } from "../../helpers/api";

export default function Index({ response }) {
  return <CatalogScreen categories={response} />;
}

export async function getServerSideProps({ query, req }) {
  const response = await api.categories.fetch();
  return {
    props: { response },
  };
}
