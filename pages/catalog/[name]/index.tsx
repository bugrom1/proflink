import { Products } from "../../../components/bus/catalogs/products/Products";
import { api } from "../../../helpers/api";

export default function Index({ response }) {
  return <Products categories={response} />;
}
export async function getServerSideProps({ query, req }) {
  const response = await api.products.fetch(query.name);
  return {
    props: { response },
  };
}
