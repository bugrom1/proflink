import { Product } from "../../../components/bus/catalogs/products/product/Product";
import { api } from "../../../helpers/api";

export default function CardCatalog({ response, query }) {
  return <Product products={response} query={query} />;
}

export async function getServerSideProps({ query, req }) {
  const response = await api.product.fetch(query.id);
  return {
    props: { response, query },
  };
}
