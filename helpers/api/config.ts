import axios from "axios";

export const apiConfig = axios.create({
  baseURL: "https://proflink.herokuapp.com/api/",
});
export const apiImg = "https://proflink.herokuapp.com";
