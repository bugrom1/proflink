import { apiConfig } from "./config";

export const api = Object.freeze({
  products: {
    fetch: async (category: string) => {
      try {
        const res = await apiConfig.get(`/products/${category}`);
        return res.data;
      } catch (e) {
        return { error: e.message };
      }
    },
  },
  product: {
    fetch: async (id: string) => {
      try {
        const res = await apiConfig.get(`/products/product/${id}`);
        return res.data;
      } catch (e) {
        return { error: e.message };
      }
    },
  },
  categories: {
    fetch: async () => {
      try {
        const res = await apiConfig.get(`/categories`);
        return res.data;
      } catch (e) {
        return { error: e.message };
      }
    },
  },
  decisions: {
    fetch: async () => {
      try {
        const res = await apiConfig.get(`/decisions`);
        return res.data;
      } catch (e) {
        return { error: e.message };
      }
    },
  },
  decision: {
    fetch: async (id) => {
      try {
        const res = await apiConfig.get(`/decisions/${id}`);
        return res.data;
      } catch (e) {
        return { error: e.message };
      }
    },
  },
});
