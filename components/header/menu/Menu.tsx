import React from "react";
import style from "./menu.module.css";
import Link from "next/link";

export const Menu = () => {
  const menu = [
    { href: "/decision", title: "Решения" },
    { href: "/catalog", title: "Каталог" },
    { href: "/portfolio", title: "Портфолио" },
    { href: "/about", title: "О Нас" },
    { href: "/contacts", title: "Контакты" },
    { href: "/calculator", title: "Калькулятор" },
  ];
  const menuElement = menu.map((el) => (
    <li key={el.href + el.title} className={style.menuElement}>
      <Link href={el.href}>
        <a>{el.title}</a>
      </Link>
    </li>
  ));
  return (
    <div className={style.menu}>
      <nav className={style.nav} tabIndex={1}>
        <ul>{menuElement}</ul>
      </nav>
    </div>
  );
};
