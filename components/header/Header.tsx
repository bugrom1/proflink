import React, { FC, useEffect, useState } from "react";
import { AppBar, Container, IconButton } from "@material-ui/core";
import { useSelector } from "react-redux";
import Badge from "@material-ui/core/Badge";
import { Logo } from "../../elements/logo/Logo";
import { PhoneBlock } from "../../elements/phoneBlock/PhoneBlock";
import { Menu } from "./menu/Menu";
import style from "./header.module.css";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import { useRouter } from "next/router";
import { AppState } from "../../init/rootReducer";

export const Header: FC = () => {
  const router = useRouter();
  const { cartItems } = useSelector((state: AppState) => state.cart);
  const [count, setCount] = useState("");
  useEffect(() => {
    setCount(cartItems.length);
  }, [cartItems]);
  const goToCart = () => {
    router.push("/cart");
  };
  return (
    <AppBar color={"transparent"} position={"sticky"}>
      <Container maxWidth={false} className={style.headerUp}>
        <Container fixed>
          <div className={style.wrapperHeaderUp}>
            <Logo />
            <div className={style.wrapperPhone}>
              <PhoneBlock />
              <IconButton aria-label="cart" onClick={goToCart}>
                <Badge badgeContent={count} className={style.badge}>
                  <ShoppingCartIcon fontSize={"large"} />
                </Badge>
              </IconButton>
            </div>
          </div>
        </Container>
      </Container>
      <Container maxWidth={false} className={style.headerDown}>
        <Container fixed>
          <Menu />
        </Container>
      </Container>
    </AppBar>
  );
};
