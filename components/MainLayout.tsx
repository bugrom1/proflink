import React, { FC, ReactNode } from "react";
import Head from "next/head";
import { Footer } from "./footer/Footer";
import { Header } from "./header/Header";
import style from "./mainLayout.module.css";

type typeProps = {
  children?: ReactNode;
  title?: string;
};

export const MainLayout: FC<typeProps> = ({ children, title = "ProfLink" }) => {
  return (
    <>
      <Head>
        <title>{title}</title>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500&display=swap"
          rel="stylesheet"
        />
      </Head>
      <div className={style.container}>
        <div>
          <Header />
          <main>{children}</main>
        </div>
        <Footer />
      </div>
    </>
  );
};
