import { Slide } from "react-slideshow-image";
import style from "./slider.module.css";
import "react-slideshow-image/dist/styles.css";
import { FC } from "react";
import Image from "next/image";
import { Grid } from "@material-ui/core";
import { ArdProduction } from "../bus/catalogs/products/cardProduction/СardProduction";

type typeProps = {
  img?: string[];
  components?: any;
};

export const Slider: FC<typeProps> = ({ img, components }: typeProps) => {
  if (img) {
    return (
      <div>
        <Slide easing="ease">
          {img.map((el) => (
            <div key={el} className={style.cart}>
              <Image
                src={el}
                alt="Picture of the author"
                layout="intrinsic"
                width="1000"
                height="500"
              />
            </div>
          ))}
        </Slide>
      </div>
    );
  } else if (components) {
    return (
      <div>
        <Slide easing="ease">
          <div className={style.cart}>
            <Grid container>
              <ArdProduction
                img={""}
                id="1"
                price={123}
                title="IP-camers 22"
                categoriesName="Видеонаблюдение"
              />
            </Grid>
            <Grid container>
              <ArdProduction
                img={""}
                id="1"
                price={123}
                title="IP-camers 22"
                categoriesName="Видеонаблюдение"
              />
            </Grid>
          </div>
          <div className={style.cart}>
            <Grid container>
              <ArdProduction
                img={""}
                id="1"
                price={123}
                title="IP-camers 22"
                categoriesName="Видеонаблюдение"
              />
            </Grid>
            <Grid container>
              <ArdProduction
                img={""}
                id="1"
                price={123}
                title="IP-camers 22"
                categoriesName="Видеонаблюдение"
              />
            </Grid>
          </div>
        </Slide>
      </div>
    );
  } else {
    return (
      <div>
        <Slide easing="ease">
          <div className={style.cart}>
            <div className={style.cars}></div>
            <div className={style.cars}></div>
          </div>
          <div className={style.cart}>slide2</div>
          <div className={style.cart}>slide3</div>
        </Slide>
      </div>
    );
  }
};
