import React, { FC } from "react";
import style from "./footer.module.css";
import { PhoneBlock } from "../../elements/phoneBlock/PhoneBlock";

export const Footer: FC = () => {
  return (
    <footer className={style.footer}>
      <div className={style.wrapper}>
        <p className={style.copyright}>2021 Все права защищены "ProfLink"</p>
        <PhoneBlock />
      </div>
    </footer>
  );
};
