import style from "./blockOurAdvantages.module.css";
import { FC } from "react";

type typeProps = {
  text: string;
  titleText: string;
};

export const BlockOurAdvantages: FC<typeProps> = ({
  titleText,
  text,
}: typeProps) => {
  return (
    <div className={style.block}>
      <p className={style.titleText}>{titleText}</p>
      <p className={style.text}>{text}</p>
    </div>
  );
};
