import { Container, Grid } from "@material-ui/core";
import style from "./about.module.css";
import { TextMain } from "../../../elements/textMain/TextMain";
import { PhotoElement } from "../../../elements/photoElement/PhotoElement";
import { MainLayout } from "../../MainLayout";
import { FC } from "react";
import { BlockOurAdvantages } from "./blockOurAdvantages/BlockOurAdvantages";

export const AboutScreen: FC = () => {
  const certificate = [
    "/static/images/Attestat_na_elektriku_1_1x.jpg",
    "/static/images/Litsenziya_1_1x.jpg",
    "/static/images/sm_Sv-vo_o_registratsii_1x.jpg",
    "/static/images/Svidetelstvo_o_tekhkompetentsii_1_1x.jpg",
    "/static/images/Svidetelstvo_o_tekhkompetentsii_2_1x.jpg",
  ];
  return (
    <MainLayout>
      <Container maxWidth={"lg"}>
        <div className={style.wrapper}>
          <Grid container spacing={3}>
            <TextMain text="О Нас" />
            <Grid item xs={12}>
              <div className={style.wrapperImg}>
                <div className={style.shadow}></div>
              </div>
            </Grid>
            <TextMain text="О Компании" />
            <Grid item xs={12}>
              <p className={style.textInfo}>
                Основными направлениями деятельности компании являются
                разработка и внедрение решений в области информационных
                технологий, телекоммуникаций и информационной безопасности. Наши
                услуги востребованы у тех, кто постоянно развивает свой бизнес и
                выводит его на новый уровень. Основное место творчества - Гродно
                и Гродненская область, но выполняем работы по все территории
                Республики Беларусь.
              </p>
              <BlockOurAdvantages
                text=" Мы придерживаемся принципа постоянного обучения всех
                сотрудников. Наша цель быть на пике технологического развития и
                предлагать нашим любимым клиентам только современные решения."
                titleText="наши специалисты"
              />
              <BlockOurAdvantages
                text=" Мы неуклонно прилагаем усилия по обеспечению персонала
                качественным профессиональным оборудованием и инструментом
                ведущих мировых производителей."
                titleText="наше обородование"
              />
              <BlockOurAdvantages
                text="Нам интересны не только прибыль, но и опыт, навыки и
                мастерство, которые мы приобретаем, реализуя новые, все более
                сложные проекты, удовлетворяя самым жестким требованиям клиентов
                и партнеров."
                titleText="наши интересы"
              />
            </Grid>
            <TextMain text="Сертификаты" />
            {certificate.map((el) => (
              <PhotoElement key={el} src={el} />
            ))}
          </Grid>
        </div>
      </Container>
    </MainLayout>
  );
};
