import { FC } from "react";
import { Container, Grid } from "@material-ui/core";
import style from "../../../styles/index.module.css";
import { BlockMain } from "./blockMain/BlockMain";
import { BlockDecision } from "./blockDecision/BlockDecision";
import { BlockPortfolio } from "./blockPortfolio/BlockPortfolio";
import { BlockAbout } from "./blockAbout/BlockAbout";
import { MainLayout } from "../../MainLayout";

export const HomeScreen: FC = () => {
  return (
    <MainLayout>
      <Container maxWidth={"lg"}>
        <div className={style.wrapper}>
          <Grid container spacing={6}>
            <BlockMain />
            <BlockDecision />
            <BlockPortfolio />
            <BlockAbout />
          </Grid>
        </div>
      </Container>
    </MainLayout>
  );
};
