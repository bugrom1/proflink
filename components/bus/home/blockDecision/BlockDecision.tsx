import { TextMain } from "../../../../elements/textMain/TextMain";
import { Grid } from "@material-ui/core";
import style from "./blockDecision.module.css";
import { Card } from "../../../../elements/card/Card";
import { Button } from "../../../../elements/button/Button";

export const BlockDecision = () => {
  return (
    <>
      <TextMain text="Популярные Решения" />
      <Card link="/decision" id="2" text="Решение" />
      <Card link="/decision" id="2" text="Решение" />
      <Card link="/decision" id="2" text="Решение" />
      <Card link="/decision" id="2" text="Решение" />
      <Grid item xs={12}>
        <div className={style.wrapperButton}>
          <Button title="Больше Решений" href="/decision" />
        </div>
      </Grid>
    </>
  );
};
