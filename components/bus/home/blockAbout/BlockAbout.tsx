import { Logo } from "../../../../elements/logo/Logo";
import { TextMain } from "../../../../elements/textMain/TextMain";
import style from "./blockAbout.module.css";
import { Grid } from "@material-ui/core";
import { Button } from "../../../../elements/button/Button";

export const BlockAbout = () => {
  return (
    <>
      <TextMain text="О Нас" />
      <Grid item xs className={style.logo}>
        <div className={style.wrapper}>
          <Logo />
        </div>
      </Grid>
      <Grid item xs={10} className={style.about}>
        <div className={style.wrapper}>
          <p className={style.text}>
            Основными направлениями деятельности компании являются разработка и внедрение решений в области информационных технологий, телекоммуникаций и информационной безопасности. Наши услуги востребованы у тех, кто постоянно развивает свой бизнес и выводит его на новый уровень. Основное место творчества - Гродно и Гродненская область, но выполняем работы по все территории Республики Беларусь.
          </p>
        </div>
      </Grid>
      <Grid item xs={12}>
        <div className={style.wrapperButton}>
          <Button title="Больше О Нас" href="/about" />
        </div>
      </Grid>
    </>
  );
};
