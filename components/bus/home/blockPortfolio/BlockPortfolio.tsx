import { Slider } from "../../../slaider/slider";
import { TextMain } from "../../../../elements/textMain/TextMain";
import { Grid } from "@material-ui/core";
import style from "./blockPortfolio.module.css";
import { Button } from "../../../../elements/button/Button";

export const BlockPortfolio = () => {
  return (
    <>
      <TextMain text="Портфолио" />
      <Grid item xs={12} className={style.slider}>
        <div className={style.wrapperSlider}>
          <Slider
            img={[
              "/static/images/6c285f12.jpg",
              "/static/images/dizayn_ofisa_2_06233927.jpg",
            ]}
          />
        </div>
      </Grid>
      <Grid item xs={12}>
        <div className={style.wrapperButton}>
          <Button title="Больше Работ" href="/portfolio" />
        </div>
      </Grid>
    </>
  );
};
