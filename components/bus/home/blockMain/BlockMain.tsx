import { Grid } from "@material-ui/core";
import { Slider } from "../../../slaider/slider";
import style from "./blockMain.module.css";
import { FC } from "react";
import { Button } from "../../../../elements/button/Button";

export const BlockMain: FC = () => {
  return (
    <>
      <Grid item xs={12} sm={6}>
        <div className={style.info}>
          <h1 className={style.title}>
            Системы безопасности и телекоммуникаций
          </h1>
          <Button href="/decision" title="К Решениям" />
        </div>
      </Grid>
      <Grid item xs={12} sm={6} className={style.slider}>
        <Slider components />
      </Grid>
    </>
  );
};
