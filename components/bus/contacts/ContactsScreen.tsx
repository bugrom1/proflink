import { Container, Grid } from "@material-ui/core";
import style from "./contacts.module.css";
import { TextMain } from "../../../elements/textMain/TextMain";
import { BlockInfo } from "./blockInfo/blockInfo";
import { Maps } from "./maps/Maps";
import { MainLayout } from "../../MainLayout";
import { FC } from "react";

export const ContactsScreen: FC = () => {
  return (
    <MainLayout>
      <Container maxWidth={"lg"}>
        <div className={style.wrapper}>
          <Grid container spacing={4}>
            <TextMain text="Контакты" />
            <Grid item xs={12}>
              <div className={style.wrapperImg}>
                <div className={style.shadow}></div>
              </div>
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <div className={style.wrapperInfo}>
                <BlockInfo title="Адрес:" text={["г.Гродно ул.Суворово 127"]} />
                <BlockInfo
                  title="Время работы:"
                  text={["C 09:00 до 17:30 - Будние (без обеда)"]}
                />
              </div>
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <div className={style.wrapperInfo}>
                <BlockInfo title="Телефоны:" text={["+375 (29) 1-688-299", "+375 (33) 688-28-99"]} />
                <BlockInfo title="Email:" text={["profLink@gmail.com"]} />
              </div>
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <div className={style.wrapperInfo}>
                <BlockInfo
                  text={[
                    "ООО ProfLink УНП 691835735 Счет в BYN:BY47ALFA3FG222512567340270000 в ЗАО «Альфа-Банк», 220030, г.Минск, что то там",
                  ]}
                />
              </div>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <div className={style.wrapperMap}>
              <Maps />
            </div>
          </Grid>
        </div>
      </Container>
    </MainLayout>
  );
};
