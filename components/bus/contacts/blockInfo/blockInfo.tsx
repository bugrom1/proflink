import style from "./blockInfo.module.css";
import { FC } from "react";

type typeProps = {
  title?: string;
  text: string[];
};

export const BlockInfo: FC<typeProps> = ({ title, text }: typeProps) => {
  return (
    <div className={style.blockInfo}>
      <h2>{title}</h2>
      {text.map((el) => (
        <p key={el}>{el}</p>
      ))}
    </div>
  );
};
