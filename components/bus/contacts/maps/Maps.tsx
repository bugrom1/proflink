import style from "./maps.module.css";
import { YMaps, Map, Placemark } from "react-yandex-maps";

const mapData = {
  center: [53.677834, 23.829529],
  zoom: 12,
};

const coordinates = [[53.656671, 23.80202]];

export const Maps = () => (
  <div className={style.maps}>
    <YMaps>
      <Map className={style.map} defaultState={mapData}>
        {coordinates.map((coordinate, index) => (
          <Placemark key={coordinate.toString()} geometry={coordinate} />
        ))}
      </Map>
    </YMaps>
  </div>
);
