import { Dispatch } from "redux";
import { AppState } from "../../../init/rootReducer";
import { CART_ADD_ITEM, CART_REMOVE_ITEM } from "./types";
import { api } from "../../../helpers/api";

export const addToCart = (id: string, qty: string) => async (
  dispatch: Dispatch,
  getState: any
) => {
  const data = await api.product.fetch(id);
  console.log(data)
  dispatch({
    type: CART_ADD_ITEM,
    payload: {
      product: data._id,
      name: data.name,
      img: data.img,
      price: data.price,
      countInStock: data.countInStock,
      translation: data.translation,
      qty,
    },
  });
  localStorage.setItem("cartItems", JSON.stringify(getState().cart.cartItems));
};

export const removeItem = (payload: string) => async (
  dispatch: Dispatch,
  getState: any
) => {
  dispatch({
    type: CART_REMOVE_ITEM,
    payload,
  });
  localStorage.setItem("cartItems", JSON.stringify(getState().cart.cartItems));
};
