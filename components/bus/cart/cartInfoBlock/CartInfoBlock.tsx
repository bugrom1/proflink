import { FC } from "react";
import { Grid, Paper } from "@material-ui/core";
import style from "./cartInfoBlock.module.css";

type propsType = {
  purchases: number;
  price: number;
  getOrder: () => void;
  disabled: boolean;
};

export const CartInfoBlock: FC<propsType> = ({
  price,
  purchases,
}: propsType) => {
  return (
    <Grid item xs>
      <Paper className={style.subtotalItem}>
        <div className={style.wrapperText}>
          <p className={style.text}>Количеств товаров</p>
          <p className={style.text}>{purchases}</p>
        </div>
        <div className={style.wrapperText}>
          <p className={style.text}>Итого</p>
          <p className={style.text}>{price} BYN</p>
        </div>
      </Paper>
    </Grid>
  );
};
