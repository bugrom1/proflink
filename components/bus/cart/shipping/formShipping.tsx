import { ErrorMessage, Field, Form } from "formik";
import { FC } from "react";
import style from "./formShipping.module.css";

type typeProps = {
  id: string;
  placeholder: string;
  type: string;
};

export const FormShipping: FC<typeProps> = ({
  id,
  placeholder,
  type,
}: typeProps) => {
  return (
    <div className={style.form}>
      <label className={style.label} htmlFor={id}>{placeholder}</label>
      <Field id={id} name={id} type={type} placeholder={placeholder} />
      <ErrorMessage name={id}>{(msg) => <p className={style.error}>{msg}</p>}</ErrorMessage>
    </div>
  );
};
