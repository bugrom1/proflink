import { useRouter } from "next/router";
import { Grid, Paper } from "@material-ui/core";
import style from "./shipping.module.css";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import { FormShipping } from "./formShipping";
import { FC } from "react";

export const Shipping: FC = () => {
  const router = useRouter();
  const initialState = {
    name: "",
    contactPerson: "",
    email: "",
    bankDetails: "",
    tel: "",
  };
  const inputEl = [
    {
      id: "name",
      placeholder: "Наименование юр. лица, инд. предпринимателя:",
      type: "text",
    },
    { id: "contactPerson", placeholder: "Контактное лицо:", type: "text" },
    { id: "bankDetails", placeholder: "Реквизиты:", type: "text" },
    { id: "email", placeholder: "Email:", type: "email" },
    { id: "tel", placeholder: "Контактный телефон:", type: "tel" },
  ];
  return (
    <div className={style.wrapper}>
      <Grid container spacing={3}>
        <Grid className={style.gridWrapper} item xs={12}>
          <Paper className={style.formWrapper}>
            <Formik
              initialValues={initialState}
              onSubmit={(values) => {
                console.log(values);
              }}
              validationSchema={Yup.object({
                name: Yup.string().required("Обязательное поле"),
                lastName: Yup.string().required("Обязательное поле"),
                email: Yup.string().required("Обязательное поле"),
                address: Yup.string().required("Обязательное поле"),
                tel: Yup.number().required("Обязательное поле"),
              })}
            >
              <Form onChange={(e) => e.preventDefault()}>
                {inputEl.map((el) => (
                  <FormShipping
                    key={el.id}
                    type={el.type}
                    id={el.id}
                    placeholder={el.placeholder}
                  />
                ))}
                <button className={style.button} type="submit">
                  Оформить заказ
                </button>
              </Form>
            </Formik>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};
