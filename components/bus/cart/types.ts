import { typeCartItem } from "./cartReducer";

export const CART_ADD_ITEM = "CART_ADD_ITEM";
export type cartAddItem = {
  type: typeof CART_ADD_ITEM;
  payload: typeCartItem;
};

export const CART_REMOVE_ITEM = "CART_REMOVE_ITEM";
export type cartRemoveItem = {
  type: typeof CART_REMOVE_ITEM;
  payload: string;
};

export type CartAction = cartAddItem | cartRemoveItem;
