import { FC, useState } from "react";
import { IconButton, Paper } from "@material-ui/core";
import style from "./cartItem.module.css";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import Link from "next/link";
import { Selector } from "../../../../elements/selector/Selector";
import { apiImg } from "../../../../helpers/api/config";

type typeProps = {
  href: string;
  price: string;
  name: string;
  img: string;
  qrt: string;
  id: string;
  deleteItem: (id: string) => void;
  getNewQrt: (id: string, value: string) => void;
  countInStock: string;
};

export const CartItem: FC<typeProps> = ({
  href,
  price,
  name,
  qrt,
  img,
  id,
  deleteItem,
  getNewQrt,
  countInStock,
}: typeProps) => {
  const qrtChange = (value) => {
    getNewQrt(id, value);
  };
  const stock = new Array(countInStock)
    .fill("")
    .map((el, index) => el + (index + 1));
  const deleteProduct = () => {
    deleteItem(id);
  };
  return (
    <Paper className={style.wrapper}>
      <div className={style.wrapperImg}>
        <img src={`${apiImg}${img}`} alt={name} />
      </div>
      <div className={style.wrapperBlock}>
        <Link href={href}>
          <a>{name}</a>
        </Link>
        <div className={style.blockPriceAndDelete}>
          <p className={style.price}>
            {price} x {qrt} = {Number(price) * Number(qrt)} BYN
          </p>
          <Selector value={Number(qrt)} params={stock} onChange={qrtChange} />
          <IconButton onClick={deleteProduct}>
            <DeleteForeverIcon fontSize="large" />
          </IconButton>
        </div>
      </div>
    </Paper>
  );
};
