import { useEffect, useState } from "react";
import { AppState } from "../../../../init/rootReducer";
import { useSelector } from "react-redux";

export const useInfoCart = () => {
  const [count, setCount] = useState(0);
  const [purchases, setPurchases] = useState(0);
  const [price, setPrice] = useState(0);
  const { cartItems } = useSelector((state: AppState) => state.cart);
  const allItems = cartItems.reduce((acc, item) => acc + Number(item.qty), 0);
  const sum = cartItems
    .reduce((acc, item) => acc + Number(item.price) * Number(item.qty), 0)
    .toFixed(2);
  useEffect(() => {
    if (cartItems.length !== 0) {
      setCount(cartItems.length);
    }
    setPurchases(allItems);
    setPrice(sum);
  }, [cartItems]);

  return {
    count,
    purchases,
    price,
    cartItems,
  };
};
