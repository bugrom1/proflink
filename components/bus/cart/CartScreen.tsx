import { Container, Grid } from "@material-ui/core";
import style from "./cart.module.css";
import { useDispatch } from "react-redux";
import { TextMain } from "../../../elements/textMain/TextMain";
import { MainLayout } from "../../MainLayout";
import { useRouter } from "next/router";
import { FC } from "react";
import { CartItem } from "./cartItem/CartItem";
import { useInfoCart } from "./hooks/useInfoCart";
import { CartInfoBlock } from "./cartInfoBlock/CartInfoBlock";
import { addToCart, removeItem } from "./action";
import { Shipping } from "./shipping/Shipping";

export const CartScreen: FC = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const { purchases, count, cartItems, price } = useInfoCart();
  const disabled = cartItems.length === 0;
  const getOrder = () => {
    router.push("/shipping");
  };
  const getNewQrt = (id: string, value: string) => {
    dispatch(addToCart(id, value));
  };
  const deleteItem = (id: string) => {
    dispatch(removeItem(id));
  };
  const renderCartItems =
    count !== 0 ? (
      cartItems.map((el) => (
        <CartItem
          key={el.product}
          id={el.product}
          deleteItem={deleteItem}
          getNewQrt={getNewQrt}
          price={el.price}
          img={el.img}
          href={`/catalog/${el.translation}/${el.product}`}
          name={el.name}
          qrt={el.qty}
          countInStock={el.countInStock}
        />
      ))
    ) : (
      <p>Здесь пока что нету товаров</p>
    );
  return (
    <MainLayout title="ProfLink | Корзина">
      <Container maxWidth={"lg"}>
        <div className={style.wrapper}>
          <Grid container spacing={3}>
            <TextMain text="Корзина" />
            <Grid item xs={6} className={style.wrapperItems}>
              {renderCartItems}
              <CartInfoBlock
                disabled={disabled}
                price={price}
                getOrder={getOrder}
                purchases={purchases}
              />
            </Grid>
            <Grid item xs>
              <Shipping />
            </Grid>
          </Grid>
        </div>
      </Container>
    </MainLayout>
  );
};
