import { CatalogAction, GET_SUBCATEGORIES } from "./types";

type subcategories = {
  id: string;
  title: string;
};

export type categories = {
  id: string;
  title: string;
  translation: string;
  allSubcategories: subcategories[];
};

type initialType = {
  allCategories: categories[];
};

const initialState = {
  allCategories: [
    {
      id: "",
      title: "",
      translation: "",
      allSubcategories: [
        {
          id: "",
          title: "",
        },
      ],
    },
  ],
};

export const catalogReducer = (
  state: initialType = initialState,
  action: CatalogAction
) => {
  switch (action.type) {
    default:
      return state;
  }
};
