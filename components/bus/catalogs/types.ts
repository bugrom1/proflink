import { categories } from "./catalogReducer";

export const SAVE_CATEGORIES = "SAVE_CATEGORIES";
export type saveCategories = {
  type: typeof SAVE_CATEGORIES;
  payload: categories[];
};

export const GET_SUBCATEGORIES = "GET_SUBCATEGORIES";
export type getSubcategories = {
  type: typeof GET_SUBCATEGORIES;
  payload: { id: string };
};

export type CatalogAction = saveCategories | getSubcategories;
