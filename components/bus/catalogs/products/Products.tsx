import { Container, Grid } from "@material-ui/core";
import style from "./products.module.css";
import { TextMain } from "../../../../elements/textMain/TextMain";
import { Filter } from "./filter/Filter";
import { MainLayout } from "../../../MainLayout";
import { FC } from "react";
import { Breadcrumb } from "../../../../elements/breadcrumb/Breadcrumb";
import { ArdProduction } from "./cardProduction/СardProduction";

type category = {
  category: string;
  countInStock: number;
  createdAt: string;
  description: string;
  image: string;
  name: string;
  price: number;
  translation: string;
  updatedAt: string;
  _id: string;
  img: string;
};

type typeProps = {
  categories: category[];
};

export const Products: FC<typeProps> = ({ categories }: typeProps) => {
  const categoryNull = categories.length === 0;
  const categoryName = categoryNull
    ? "Такой категории нету"
    : categories[0].category;
  const breadcrumb = [
    { title: "Каталог", href: `/catalog` },
    {
      title: `${
        categoryNull ? "Такой категории нету" : categories[0].category
      }`,
      href: "",
    },
  ];
  //@ts-ignore
  const error = categories.error ? <p>Упс</p> : null;
  const renderCard =
    error ||
    categories.map((el) => (
      <ArdProduction
        img={el.img}
        title={el.name}
        price={el.price}
        key={el._id}
        categoriesName={el.translation}
        id={el._id}
      />
    ));
  return (
    <MainLayout title={`ProfLink | ${categoryName}`}>
      <Container maxWidth={"lg"}>
        <div className={style.wrapper}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Breadcrumb links={breadcrumb} />
            </Grid>
            <TextMain
              text={
                categoryNull
                  ? "Такой категории нету"
                  : `${categories[0].category}`
              }
            />
            <Grid item xs={12}>
              <Grid container spacing={4}>
                {renderCard}
              </Grid>
            </Grid>
          </Grid>
        </div>
      </Container>
    </MainLayout>
  );
};
