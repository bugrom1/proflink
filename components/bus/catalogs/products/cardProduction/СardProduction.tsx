import { Grid, Paper } from "@material-ui/core";
import style from "./cardProduction.module.css";
import { FC } from "react";
import { useRouter } from "next/router";
import { apiImg } from "../../../../../helpers/api/config";

type typeProps = {
  title: string;
  price: number;
  categoriesName: string;
  id: string;
  img: string;
};

export const ArdProduction: FC<typeProps> = ({
  title,
  price,
  categoriesName,
  id,
  img,
}: typeProps) => {
  const router = useRouter();
  const linkClickHandler = () => {
    router.push(`/catalog/${categoriesName}/${id}`);
  };
  return (
    <Grid item xs={12} sm={6} md={3}>
      <Paper onClick={linkClickHandler} className={style.wrapper}>
        <img className={style.img} src={`${apiImg}${img}`} alt="" />
        <h3 className={style.title}>{title}</h3>
        <div className={style.blockCost}>
          <p className={style.price}>{price}</p>
          <p className={style.price}>Бел руб</p>
        </div>
      </Paper>
    </Grid>
  );
};
