import React, { FC } from "react";
import { makeStyles, Theme } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import style from "./tabsProductCard.module.css";

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
  label: string;
}

function TabPanel(props: TabPanelProps) {
  const { label, children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <div className={style.wrapper}>
          <table className={style.table}>
            <thead>
              <tr className={style.tableTr}>
                <th className={style.tableTh}>{label}</th>
              </tr>
            </thead>
            <tbody>{children}</tbody>
          </table>
        </div>
      )}
    </div>
  );
}

function a11yProps(index: any) {
  return {
    id: `scrollable-auto-tab-${index}`,
    "aria-controls": `scrollable-auto-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

type propsType = {
  tabs: {
    label: string;
    description:
      | { specifications: string; parameter: string; _id: string }[]
      | string;
  }[];
};

export const TabsProductCard: FC<propsType> = ({ tabs }: propsType) => {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  return (
    <div className={style.root}>
      <div className={style.wrapperTabs}>
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
          aria-label="scrollable auto tabs example"
        >
          {tabs.map((el, index) => (
            <Tab key={el.label} label={el.label} {...a11yProps(index)} />
          ))}
        </Tabs>
      </div>
      {tabs.map((el, index) => (
        <TabPanel
          label={el.label}
          key={Math.random()}
          value={value}
          index={index}
        >
          {Array.isArray(el.description)
            ? el.description.map((el) => (
                <tr key={Math.random()} className={style.tableTr}>
                  <td className={style.tableTd}>{el.specifications}</td>
                  <td className={style.tableTd}>{el.parameter}</td>
                </tr>
              ))
            : el.description}
        </TabPanel>
      ))}
    </div>
  );
};
