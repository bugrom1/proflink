import { ChangeEvent, FC } from "react";
import { useFormik } from "formik";
import style from "./filter.module.css";

type typeProps = {
  subcategories: any;
};

export const Filter: FC<typeProps> = ({ subcategories }: typeProps) => {
  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {};

  const formik = useFormik({
    initialValues: {
      checked: [],
    },
    onSubmit: (values) => {
      alert(JSON.stringify(values, null, 2));
    },
  });

  return (
    <div>
      <p className={style.text}>Категории</p>
      <form onSubmit={formik.handleSubmit}>
        {subcategories.map((el) => (
          <div key={el.title} className={style.filterElement}>
            <input
              id={el.title}
              name="checked"
              type="checkbox"
              value={`${el.title}`}
              onChange={formik.handleChange}
            />
            <label htmlFor={el.title}>{el.title}</label>
          </div>
        ))}
        <button type="submit">Найти</button>
      </form>
    </div>
  );
};
