import { Container, Grid } from "@material-ui/core";
import style from "./product.module.css";
import { useDispatch } from "react-redux";
import { Breadcrumb } from "../../../../../elements/breadcrumb/Breadcrumb";
import { Slider } from "../../../../slaider/slider";
import { MainLayout } from "../../../../MainLayout";
import { FC, useState } from "react";
import { TabsProductCard } from "../cardProduction/tabsProductCard/TabsProductCard";
import { addToCart } from "../../../cart/action";
import { Selector } from "../../../../../elements/selector/Selector";
import { Button } from "../../../../../elements/button/Button";
import { apiImg } from "../../../../../helpers/api/config";

type typeProps = {
  products: any;
  query: any;
};

export const Product: FC<typeProps> = ({ products, query }: typeProps) => {
  const dispatch = useDispatch();
  const product = products;
  const [qrt, setQrt] = useState("1");
  const [error, setError] = useState("");
  const tabs = [];
  if (product.specifications) {
    tabs.push(product.specifications);
  }
  if (product.globalDescription) {
    tabs.push(product.globalDescription);
  }
  const t = [
    { title: "Каталог", href: `/catalog` },
    { title: product.category, href: `/catalog/${product.translation}` },
    { title: product.name, href: `/${product.id}` },
  ];
  const addItemToCart = () => {
    if (!qrt) {
      setError("Количество не может быть пустым");
    }
    dispatch(addToCart(product._id, qrt));
  };
  const qrtChange = (value) => {
    setQrt(value);
    setError("");
  };
  const errors = product.error ? <p>Упс</p> : null;
  const stock = new Array(product.countInStock)
    .fill("")
    .map((el, index) => el + (index + 1));
  return (
    <MainLayout>
      <Container maxWidth={"lg"}>
        <div className={style.wrapper}>
          <Grid container spacing={3}>
            {errors ? (
              errors
            ) : (
              <>
                <Grid item xs={12}>
                  <Breadcrumb links={t} />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <div className={style.blockImg}>
                    <img
                      className={style.img}
                      src={`${apiImg}${product.img}`}
                      alt=""
                    />
                  </div>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <h3 className={style.blockInfoName}>{product.name}</h3>
                  <div className={style.blockCost}>
                    <p className={style.number}>{product.price}</p>
                    <p className={style.number}>бел руб</p>
                  </div>
                  <p className={style.place}>
                    На складе {product.countInStock}
                  </p>
                  {error ? <p className={style.errorMessage}>{error}</p> : null}
                  <div className={style.blockAddCart}>
                    <Button
                      title="Добавить в корзину"
                      onClick={addItemToCart}
                    />
                    <div className={style.selectorBlock}>
                      <Selector params={stock} onChange={qrtChange} />
                    </div>
                  </div>
                  <p>{product.description}</p>
                </Grid>
                <Grid item xs={12}>
                  <div className={style.wrapperTabs}>
                    <TabsProductCard tabs={tabs} />
                  </div>
                </Grid>
              </>
            )}
          </Grid>
        </div>
      </Container>
    </MainLayout>
  );
};
