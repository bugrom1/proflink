import { Dispatch } from "redux";
import { SAVE_CATEGORIES } from "./types";
import { categories } from "./catalogReducer";

export const saveCategories = (categories: categories[]) => {
  return {
    type: SAVE_CATEGORIES,
    payload: categories,
  };
};
