import { Container, Grid } from "@material-ui/core";
import style from "./catalog.module.css";
import { TextMain } from "../../../elements/textMain/TextMain";
import { MainLayout } from "../../MainLayout";
import { FC, useState } from "react";
import { Categories } from "./categories/Categories";
import { Card } from "../../../elements/card/Card";

type typeProps = {
  categories: any;
};

export const CatalogScreen: FC<typeProps> = ({ categories }: typeProps) => {
  const [active, setActive] = useState("");
  const [sub, setSub] = useState(null);
  const getSubcategories = (id: string) => {
    const subcategories = categories.filter((el) => el._id === id);
    setActive(id);
    setSub(subcategories[0]);
  };
  const renderCategories = categories.error ? (
    <p>упс произошла ошибка</p>
  ) : (
    categories.map((el) => (
      <Categories
        active={active}
        key={el.title}
        id={el._id}
        title={el.title}
        getSubcategories={getSubcategories}
      />
    ))
  );
  const renderSubcategories = sub ? (
    sub.subcategories ? (
      sub.subcategories.map((el) => (
        <Card
          md={4}
          img={el.img}
          key={el._id}
          text={el.title}
          id={el._id.toString()}
          link={`/catalog/${el.translation}`}
        />
      ))
    ) : (
      <p>нету категорий</p>
    )
  ) : (
    <p>Лучшие товары</p>
  );
  return (
    <MainLayout>
      <Container maxWidth={"lg"}>
        <div className={style.wrapper}>
          <Grid container spacing={3}>
            <TextMain text="Каталог" />
            <Grid item xs className={style.subContainer}>
              <ul className={style.list}>{renderCategories}</ul>
            </Grid>
            <Grid item xs={8} className={style.subContainer}>
              <Grid container spacing={3}>
                {renderSubcategories}
              </Grid>
            </Grid>
          </Grid>
        </div>
      </Container>
    </MainLayout>
  );
};
