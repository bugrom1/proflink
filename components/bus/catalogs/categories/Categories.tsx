import { FC } from "react";
import style from "./categories.module.css";

type typeProps = {
  title: string;
  id: any;
  getSubcategories: (id: any) => void;
  active: string;
};
export const Categories: FC<typeProps> = ({
  title,
  id,
  getSubcategories,
  active,
}: typeProps) => {
  const onClick = () => {
    getSubcategories(id);
  };
  const styles = [style.list];
  if (active === String(id)) {
    styles.push(`${style.active}`);
  }
  return (
    <li className={styles.join(" ")} onClick={onClick}>
      <p>{title}</p>
    </li>
  );
};
