import { Container, Grid } from "@material-ui/core";
import style from "./decisions.module.css";
import { TextMain } from "../../../elements/textMain/TextMain";
import { Card } from "../../../elements/card/Card";
import { MainLayout } from "../../MainLayout";
import { FC } from "react";

type typeProps = {
  decisions: any;
};

export const DecisionsScreen: FC<typeProps> = ({ decisions }: typeProps) => {
  return (
    <MainLayout>
      <Container maxWidth={"lg"}>
        <div className={style.wrapper}>
          <Grid container spacing={3}>
            <TextMain text="Решения" />
            {decisions.map((el) => (
              <Card
                id={el._id}
                link={`/decision/${el._id}`}
                key={el._id}
                text={el.name}
                img={el.img}
              />
            ))}
          </Grid>
        </div>
      </Container>
    </MainLayout>
  );
};
