import React, { FC } from "react";
import { makeStyles, Theme } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { Grid } from "@material-ui/core";
import style from "./tabs.module.css";
import { apiImg } from "../../../../helpers/api/config";
import { Button } from "../../../../elements/button/Button";

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && <div className={style.wrapper}>{children}</div>}
    </div>
  );
}

function a11yProps(index: any) {
  return {
    id: `scrollable-auto-tab-${index}`,
    "aria-controls": `scrollable-auto-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

type propsType = {
  tabs: {
    _id: string;
    label: string;
    description: { text: string; _id: string; img: string }[];
  }[];
};

export const ScrollableTabsButtonAuto: FC<propsType> = ({
  tabs,
}: propsType) => {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  const Image = (img) => {
    return (
      <div
        className={style.image}
        style={{ backgroundImage: `url(${apiImg}${img})` }}
      ></div>
    );
  };
  const Text = (text) => {
    return <p className={style.text}>{text}</p>;
  };
  return (
    <div className={style.root}>
      <div className={style.wrapperTabs}>
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
          aria-label="scrollable auto tabs example"
        >
          {tabs.map((el, index) => (
            <Tab key={el.label} label={el.label} {...a11yProps(index)} />
          ))}
        </Tabs>
      </div>
      {tabs.map((el, index) => (
        <TabPanel key={el._id} value={value} index={index}>
          {tabs[index].description.map((el, index) => (
            <Grid
              key={el._id}
              container
              spacing={3}
              className={style.tabsWrapper}
            >
              <Grid item xs={12} sm={6} className={style.tabsBlock}>
                <div className={style.blockWrapper}>
                  {index % 2 === 0 ? Image(el.img) : Text(el.text)}
                </div>
              </Grid>
              <Grid item xs={12} sm={6}>
                <div className={style.blockWrapper}>
                  {index % 2 === 0 ? Text(el.text) : Image(el.img)}
                </div>
              </Grid>
            </Grid>
          ))}
          <Grid item xs={12} className={style.tabButton}>
            <Button title="К Каталогу" />
          </Grid>
        </TabPanel>
      ))}
    </div>
  );
};
