import { FC } from "react";
import { Container, Grid, Paper } from "@material-ui/core";
import style from "./decision.module.css";
import { Breadcrumb } from "../../../../elements/breadcrumb/Breadcrumb";
import { TextMain } from "../../../../elements/textMain/TextMain";
import { MainLayout } from "../../../MainLayout";
import { ScrollableTabsButtonAuto } from "../Tabs/Tabs";
import { ArdProduction } from "../../catalogs/products/cardProduction/СardProduction";
import { apiImg } from "../../../../helpers/api/config";

type typeProps = {
  decision: any;
};

export const Decision: FC<typeProps> = ({ decision }: typeProps) => {
  const tabs = decision.tabs;
  const links = [
    { title: "Решения", href: `/decision` },
    { title: `${decision.name}`, href: "" },
  ];
  return (
    <MainLayout>
      <Container maxWidth={"lg"}>
        <div className={style.wrapper}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Breadcrumb links={links} />
            </Grid>
            <Grid item xs={12}>
              <div
                style={{
                  background: `url(${apiImg}${decision.img}) no-repeat fixed center`,
                }}
                className={style.wrapperImg}
              ></div>
            </Grid>
            <Grid item xs={12}>
              <TextMain text={decision.title} />
              <p className={style.description}>{decision.description}</p>
            </Grid>
            <ArdProduction
              img={""}
              title="Название"
              price={231}
              categoriesName="/catalog/videonablyudenie"
              id="2"
            />
            <ArdProduction
              img={""}
              title="Название"
              price={231}
              categoriesName="/catalog/videonablyudenie"
              id="3"
            />
            <ArdProduction
              img={""}
              title="Название"
              price={231}
              categoriesName="/catalog/videonablyudenie"
              id="4"
            />
            <ArdProduction
              img={""}
              title="Название"
              price={231}
              categoriesName="/catalog/videonablyudenie"
              id="5"
            />
            <TextMain text="Разновидности и приминения" />
            <Grid item xs={12}>
              <div className={style.wrapperTabs}>
                <ScrollableTabsButtonAuto tabs={tabs} />
              </div>
            </Grid>
          </Grid>
        </div>
      </Container>
    </MainLayout>
  );
};
