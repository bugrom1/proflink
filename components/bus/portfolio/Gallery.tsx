import { TabsPortfolio } from "./TabsPortfolio/TabsPortfolio";

export const Gallery = () => {
  const photos = [
    [
      {
        src: "https://source.unsplash.com/aZjw7xI3QAA/1144x763",
        width: 4,
        height: 3,
      },
      {
        src: "https://source.unsplash.com/c77MgFOt7e0/1144x763",
        width: 4,
        height: 3,
      },
      {
        src: "https://source.unsplash.com/QdBHnkBdu4g/1144x763",
        width: 4,
        height: 3,
      },
      {
        src: "https://source.unsplash.com/1600x900/",
        width: 4,
        height: 3,
      },
    ],
    [
      {
        src: "https://source.unsplash.com/1600x900/",
        width: 4,
        height: 3,
      },
    ],
  ];
  const labels = ["Видеонаблюдение", "Точки доступа"];
  return <TabsPortfolio labels={labels} tabs={photos} />;
};
