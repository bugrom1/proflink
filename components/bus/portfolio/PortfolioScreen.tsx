import { Container, Grid } from "@material-ui/core";
import style from "./portfolio.module.css";
import { TextMain } from "../../../elements/textMain/TextMain";
import { MainLayout } from "../../MainLayout";
import { Gallery } from "./Gallery";
import { FC } from "react";

export const PortfolioScreen: FC = () => {
  return (
    <MainLayout>
      <Container maxWidth={"lg"}>
        <div className={style.wrapper}>
          <Grid container spacing={3}>
            <TextMain text="Портфолио" />
            <Grid item xs={12}>
              <Gallery />
            </Grid>
          </Grid>
        </div>
      </Container>
    </MainLayout>
  );
};
