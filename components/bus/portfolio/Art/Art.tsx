import Carousel, { Modal, ModalGateway } from "react-images";
import React, { FC, useCallback, useState } from "react";
import Gallery from "react-photo-gallery";


type typeProps = {
  photos: any;
};

export const Art: FC<typeProps> = ({ photos }: typeProps) => {
  React.useLayoutEffect = React.useEffect;
  const [currentImage, setCurrentImage] = useState(0);
  const [viewerIsOpen, setViewerIsOpen] = useState(false);
  const openLightbox = useCallback((event, { photo, index }) => {
    setCurrentImage(index);
    setViewerIsOpen(true);
  }, []);
  const closeLightbox = () => {
    setCurrentImage(0);
    setViewerIsOpen(false);
  };
  return (
    <div>
      <Gallery photos={photos} onClick={openLightbox} />
      <ModalGateway>
        {viewerIsOpen ? (
          <Modal onClose={closeLightbox}>
            <Carousel
              currentIndex={currentImage}
              //@ts-ignore
              views={photos.map((x) => ({
                ...x,
                srcset: x.src,
                caption: x.src,
              }))}
            />
          </Modal>
        ) : null}
      </ModalGateway>
    </div>
  );
};
