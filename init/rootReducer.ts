import { combineReducers } from "redux";
import { catalogReducer } from "../components/bus/catalogs/catalogReducer";
import { cartReducer } from "../components/bus/cart/cartReducer";

export const rootReducer = combineReducers({
  catalogs: catalogReducer,
  cart: cartReducer,
});
export type AppState = ReturnType<typeof rootReducer>;
