import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import { rootReducer } from "./rootReducer";

let cartItemsFromStorage: any[] | null | string;

const ISSERVER = typeof window === "undefined";

if (!ISSERVER) {
  cartItemsFromStorage = localStorage
    ? localStorage.getItem("cartItems")
    : null;
}

if (cartItemsFromStorage) {
  if (typeof cartItemsFromStorage === "string") {
    cartItemsFromStorage = JSON.parse(cartItemsFromStorage);
  }
} else {
  cartItemsFromStorage = [];
}

const initialState = {
  cart: {
    cartItems: cartItemsFromStorage,
  },
};

export const store = createStore(
  rootReducer,
  // @ts-ignore
  initialState,
  applyMiddleware(thunk)
);
