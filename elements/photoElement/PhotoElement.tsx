import { FC } from "react";
import { Grid } from "@material-ui/core";
import Image from "next/image";

type typeProps = {
    src: string
}


export const PhotoElement: FC<typeProps> = ({src}: typeProps) => {
  return (
    <Grid item xs={12} sm={6} md={4}>
      <Image
        src={src}
        layout="intrinsic"
        width={400}
        height={600}
      />
    </Grid>
  );
};
