import { Grid, Paper } from "@material-ui/core";
import { FC } from "react";
import style from "./card.module.css";
import { useRouter } from "next/router";
import { apiImg } from "../../helpers/api/config";

type typeProps = {
  text: string;
  img?: string;
  id: string;
  link: string;
  md?: number;
};

export const Card: FC<typeProps> = ({ text, img, id, link, md }: typeProps) => {
  const size = md ? md : 3;
  const router = useRouter();
  const linkClickHandler = () => {
    router.push(`${link}`);
  };
  return (
    //@ts-ignore
    <Grid item xs={12} sm={6} md={size}>
      <Paper onClick={linkClickHandler} className={style.wrapper}>
        <div className={style.img}>
          <img src={`${apiImg}${img}`} alt={text} />
        </div>
        <div>
          <p className={style.text}>{text}</p>
        </div>
      </Paper>
    </Grid>
  );
};
