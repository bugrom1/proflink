import React, { FC } from "react";
import Link from "next/link";
import style from "./logo.module.css";
import Image from "next/image";

export const Logo: FC = () => {
  return (
    <div className={style.logo}>
      <Link href="/">
        <a>
          <img className={style.img} src="/static/images/logo1.png" alt="ProfLink"/>
        </a>
      </Link>
     {/* <Link href="/">
        <a className={style.title}>ProfLink</a>
      </Link>*/}
    </div>
  );
};
