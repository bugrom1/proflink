import { FC } from "react";
import style from "./selector.module.css";

type typeProps = {
  params: string[];
  onChange: (e: string) => void;
  value?: number;
};

export const Selector: FC<typeProps> = ({
  onChange,
  value,
  params,
}: typeProps) => {
  const Change = (e: React.ChangeEvent<HTMLInputElement>) => {
    onChange(e.target.value);
  };
  return (
    <div className={style.container} onChange={Change}>
      <select className={style.select} defaultValue={value} size={1}>
        {params.map((el, index) => (
          <option key={el + index}>{el}</option>
        ))}
      </select>
    </div>
  );
};
