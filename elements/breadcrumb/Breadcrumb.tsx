import { Breadcrumbs, Typography } from "@material-ui/core";
import Link from "@material-ui/core/Link";
import { FC } from "react";

type typeProps = {
  links: link[];
};
type link = {
  title: string;
  href: string;
};

export const Breadcrumb: FC<typeProps> = ({ links }: typeProps) => {
  const breadcrumb = links.slice(0, links.length - 1);
  return (
    <Breadcrumbs aria-label="breadcrumb">
      {breadcrumb.map((el) => (
        <Link key={el.title} color="inherit" href={el.href}>
          {el.title}
        </Link>
      ))}
      <Typography color="textPrimary">{links[links.length - 1].title}</Typography>
    </Breadcrumbs>
  );
};
