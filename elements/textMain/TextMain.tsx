import { FC } from "react";
import { Grid } from "@material-ui/core";
import style from "./textMain.module.css";

type typeProps = {
  text: string;
};

export const TextMain: FC<typeProps> = ({ text }: typeProps) => {
  return (
    <Grid item xs={12}>
      <h3 className={style.textMain}>{text}</h3>
    </Grid>
  );
};
