import { FC } from "react";
import style from "./button.module.css";
import { useRouter } from "next/router";

type typeProps = {
  title: string;
  href?: string;
  onClick?: () => void;
  disabled?: boolean;
};

export const Button: FC<typeProps> = ({ title, href, onClick, disabled }) => {
  const router = useRouter();
  const linkClickHandler = () => {
    if (href) {
      router.push(href);
    } else {
      onClick();
    }
  };
  return (
    <button
      disabled={disabled}
      onClick={linkClickHandler}
      className={style.button}
    >
      {title}
    </button>
  );
};
