import React, { FC } from "react";
import PhoneIcon from "@material-ui/icons/Phone";
import style from "./phoneBlock.module.css";

export const PhoneBlock: FC = () => {
  return (
    <div className={style.phoneBlock}>
      <PhoneIcon />
      <div>
        <p className={style.mobileNumbers}>+ 375 (33) 688-28-99</p>
        <p className={style.mobileNumbers}>+ 375 (29) 1-688-299</p>
      </div>
    </div>
  );
};
